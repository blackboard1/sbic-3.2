import Vue from 'vue';
import App from './App.vue';
import router from '@/router';
import store from '@/vuex/store';

// import './registerServiceWorker';

// Main Sass file
import './assets/sass/main.scss';

// Import MomentsJS
import moment from 'moment';

Vue.filter('formatDate', (value) => {
    if (value) {
        return moment(String(value)).format('DD.MM.YYYY');
    }
});

Vue.filter('formatShortDate', (value) => {
    if (value) {
        moment.locale('de-ch');
        return moment(String(value)).format('Do MMM');
    }
});

Vue.filter('formatTime', (value) => {
    if (value) {
        moment.locale('de-ch');
        return moment(String(value)).format('H:mm');
    }
});

// -- BUEFY --
// Vue JS Components for Bulma
import Buefy from 'buefy';
Vue.use(Buefy, {
    defaultIconPack: 'fas', // Maybe change to fas (new one)
});

// Define the base api url
Vue.prototype.BACKEND_BASE_URL = process.env.VUE_APP_BACKEND_BASE_URL;
Vue.prototype.APP_BASE_URL = process.env.VUE_APP_BASE_URL;

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
