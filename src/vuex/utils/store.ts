import Vue from 'vue';
import Vuex, { MutationTree, ActionTree } from 'vuex';
import { User } from '@/models/user';

Vue.use(Vuex);

interface State {
    users: User[];
    offsetUsers: User[];
}

const getters = {
    users(users: State) {
        return users.users;
    },
    offsetUsers(offsetUsers: State) {
        return offsetUsers.offsetUsers;
    },
};

const state: {} = {
    offsetUsers : [
        {
            id: 1,
            firstname: 'Julia',
            lastname: 'Borg',
            active: true,
        },
        {
            id: 2,
            firstname: 'Laura Andrea',
            lastname: 'Camenzind',
            active: true,
        },
        {
            id: 3,
            firstname: 'Fabiana',
            lastname: 'Evans',
            active: true,
        },
        {
            id: 4,
            firstname: 'Melina',
            lastname: 'Ilar',
            active: true,
        },
        {
            id: 5,
            firstname: 'Kevin',
            lastname: 'Maher',
            active: true,
        },
        {
            id: 6,
            firstname: 'Mark',
            lastname: 'Marolf',
            active: true,
        },
        {
            id: 7,
            firstname: 'Nina',
            lastname: 'Marsolo',
            active: true,
        },
        {
            id: 8,
            firstname: 'Colin',
            lastname: 'Mazenauer',
            active: true,
        },
        {
            id: 9,
            firstname: 'Sylvie',
            lastname: 'Meyer',
            active: true,
        },
        {
            id: 10,
            firstname: 'Zolboo',
            lastname: 'Narangerel',
            active: true,
        },
        {
            id: 11,
            firstname: 'Fabienne',
            lastname: 'Nikollaj',
            active: true,
        },
        {
            id: 12,
            firstname: 'Sophia',
            lastname: 'Ochsner',
            active: true,
        },
        {
            id: 13,
            firstname: 'Sophie',
            lastname: 'Schlechtriem',
            active: true,
        },
        {
            id: 14,
            firstname: 'Morris',
            lastname: 'Turdo',
            active: true,
        },
        {
            id: 15,
            firstname: 'Alejandro',
            lastname: 'van Engelen',
            active: false,
        },
        {
            id: 16,
            firstname: 'David',
            lastname: 'Widmer',
            active: false,
        },
    ],
    users : [
        {
            id: 1,
            firstname: 'Julia',
            lastname: 'Borg',
            active: true,
        },
        {
            id: 2,
            firstname: 'Laura Andrea',
            lastname: 'Camenzind',
            active: true,
        },
        {
            id: 3,
            firstname: 'Fabiana',
            lastname: 'Evans',
            active: true,
        },
        {
            id: 4,
            firstname: 'Melina',
            lastname: 'Ilar',
            active: true,
        },
        {
            id: 5,
            firstname: 'Kevin',
            lastname: 'Maher',
            active: true,
        },
        {
            id: 6,
            firstname: 'Mark',
            lastname: 'Marolf',
            active: true,
        },
        {
            id: 7,
            firstname: 'Nina',
            lastname: 'Marsolo',
            active: true,
        },
        {
            id: 8,
            firstname: 'Colin',
            lastname: 'Mazenauer',
            active: true,
        },
        {
            id: 9,
            firstname: 'Sylvie',
            lastname: 'Meyer',
            active: true,
        },
        {
            id: 10,
            firstname: 'Zolboo',
            lastname: 'Narangerel',
            active: true,
        },
        {
            id: 11,
            firstname: 'Fabienne',
            lastname: 'Nikollaj',
            active: true,
        },
        {
            id: 12,
            firstname: 'Sophia',
            lastname: 'Ochsner',
            active: true,
        },
        {
            id: 13,
            firstname: 'Sophie',
            lastname: 'Schlechtriem',
            active: true,
        },
        {
            id: 14,
            firstname: 'Morris',
            lastname: 'Turdo',
            active: true,
        },
        {
            id: 15,
            firstname: 'Alejandro',
            lastname: 'van Engelen',
            active: false,
        },
        {
            id: 16,
            firstname: 'David',
            lastname: 'Widmer',
            active: false,
        },
    ],
};

const module = {
    namespaced: true,
    state,
    getters,
};

export default module;
