import Vue from 'vue';
import Vuex from 'vuex';

// Utils
import utils from './utils/store';

import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState()],

    modules: {
        utils,
    },
});
