import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home.vue';
import ClassList from '@/components/ClassList.vue';
import WeekList from '@/components/WeekList.vue';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/weeks',
            name: 'weeks',
            component: WeekList,
        },
        {
            path: '/class',
            name: 'classlist',
            component: ClassList,
        },
    ],
});
