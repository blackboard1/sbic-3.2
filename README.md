# Webpack based VueJS frontend with typescript
> [https://blackboard.markmarolf.com](blackboard.markmarolf.com)

## Build Setup


``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

```

## Configs
``` bash
# set the API_BASE_URL in following files
.
├.env.local
├.env.production

```

## npm dependencies
``` bash
  "dependencies": {
    "axios": "^0.18.0",
    "buefy": "^0.6.6",
    "bulma": "^0.7.1",
    "font-awesome": "^4.7.0",
    "register-service-worker": "^1.0.0",
    "vue": "^2.5.16",
    "vue-class-component": "^6.0.0",
    "vue-property-decorator": "^6.0.0",
    "vue-router": "^3.0.1",
    "vuex": "^3.0.1",
    "vuex-persistedstate": "^2.5.4"
  },
```

# Vue JS Framework

  Vue is a progressive framework for building user interfaces. Unlike other monolithic frameworks, Vue is designed from the ground up to be incrementally adoptable. The core library is focused on the view layer only, and is easy to pick up and integrate with other libraries or existing projects. On the other hand, Vue is also perfectly capable of powering sophisticated Single-Page Applications when used in combination with modern tooling and supporting libraries.
  > https://vuejs.org/

For a more detailed explanation on the modules that are used under the hood, please check out the links down below. </br>

---

By [Mark Marolf](http://markmarolf.com/)
